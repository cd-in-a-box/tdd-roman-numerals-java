# Roman Numerals

Test Driven Development example / boilerplate

See: https://codingdojo.org/kata/RomanNumerals/

## Requirements
- JDK >= 8
- Maven

## Compile application
```
mvn compile
```

## Run unit tests
```
mvn test
```

