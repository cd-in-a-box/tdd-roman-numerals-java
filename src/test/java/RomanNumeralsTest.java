import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RomanNumeralsTest {

    private RomanNumerals converter;

    @BeforeEach
    public void setUp() {
        converter = new RomanNumerals();
    }

    @Test
    public void itCanConvertArabicNumeralsToRoman() {
        assertEquals(converter.toRoman(1), "I");
        assertEquals(converter.toRoman(2), "II");
        assertEquals(converter.toRoman(3), "III");

        assertEquals(converter.toRoman(4), "IV");
        assertEquals(converter.toRoman(5), "V");
        assertEquals(converter.toRoman(6), "VI");
        assertEquals(converter.toRoman(7), "VII");
        assertEquals(converter.toRoman(9), "IX");

        assertEquals(converter.toRoman(10), "X");
        assertEquals(converter.toRoman(11), "XI");
        assertEquals(converter.toRoman(40), "XL");

        assertEquals(converter.toRoman(50), "L");
        assertEquals(converter.toRoman(55), "LV");
        assertEquals(converter.toRoman(60), "LX");

        assertEquals(converter.toRoman(3000), "MMM");
    }

    @Test
    public void itCanGetTheLargestSubtractableSequence() {
        assertEquals(10, RomanNumerals.largestArabicICanSubtract(10));
        assertEquals(10, RomanNumerals.largestArabicICanSubtract(17));

        assertEquals(9, RomanNumerals.largestArabicICanSubtract(9));
        assertEquals(5, RomanNumerals.largestArabicICanSubtract(5));

        assertEquals(500, RomanNumerals.largestArabicICanSubtract(504));

        assertEquals(900, RomanNumerals.largestArabicICanSubtract(905));
    }
}