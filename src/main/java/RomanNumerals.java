import java.util.TreeMap;

public class RomanNumerals {
    private final static TreeMap<Integer, String> romanMap = new TreeMap<>();

    static {
        romanMap.put(1, "I");
        romanMap.put(4, "IV");
        romanMap.put(5, "V");
        romanMap.put(9, "IX");
        romanMap.put(10, "X");
        romanMap.put(40, "XL");
        romanMap.put(50, "L");
        romanMap.put(90, "XC");
        romanMap.put(100, "C");
        romanMap.put(400, "CD");
        romanMap.put(500, "D");
        romanMap.put(900, "CM");
        romanMap.put(1000, "M");
    }

    public String toRoman(int arabic) {
        int largestArabicICanSubtract = largestArabicICanSubtract(arabic);
        if (arabic == largestArabicICanSubtract) {
            return romanMap.get(arabic);
        }
        return romanMap.get(largestArabicICanSubtract) + toRoman(arabic - largestArabicICanSubtract);
    }

    public static int largestArabicICanSubtract(int arabic) {
        return romanMap.floorKey(arabic);
    }
}